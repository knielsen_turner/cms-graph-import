require 'json'
require 'forwardable'

class Node
    include Enumerable
    extend Forwardable
    
    def initialize(data, factory) 
        @data = data
        @factory = factory
        @children = []
    end

    def_delegators :@children, :<<, :each, :[], :[]=
    def_delegators :@data, :to_s, :to_json

    def collect()
        returnValue = [@data]
        @children.each do |child|
            returnValue.concat(child.collect())
        end
        returnValue.uniq{|i| i['_id']}
    end
end

class GalleryNode < Node
    def initialize(data, factory)
        super(data, factory)

        # follow the graph
        @data['slides'].each do |slide|
            @children.push(@factory.create(slide['element']))
        end
    end
end

class VideoNode < Node
    def initialize(data, factory)
        super(data, factory)

        # scrub out the relatedElementsArray for now
        @data['relatedElementsArray'] = []

        # follow the graph
        @data['thumbnails'].each do |thumbnail|
            @children.push(@factory.create(thumbnail))
        end
    end
end

class ArticleNode < Node
    def initialize(data, factory)
        super(data, factory)

        # scrub out complateCoverage for now
        @data['elements'].each do |bucket, items|
            items.delete_if{ |item| item['type'] == 'interactive' && item['subtype'] == 'completeCoverage' }
        end

        # follow the graph
        @data['elements'].each do |bucket, items|
            items.each do |item| 
                @children.push(@factory.create(item))
            end
        end
    end
end

class ImageNode < Node
    def initialize(data, factory)
        super(data, factory)

        # scrub out reference properties if they exist
        @data.delete('appearance')
        @data.delete('profile')
        @data.delete('usableObject')
    end
end

class InteractiveNode < Node
    def initialize(data, factory)
        super(data, factory)

        # scrub out reference properties if they exist
        @data.delete('caption')
    end
end

class NodeFactory 
    def initialize(db)
        @db = db
    end

    def create(data) 
        case data['type']
        when 'article'
            return ArticleNode.new(data, self)
        when 'video'
            return VideoNode.new(data, self)
        when 'gallery'
            return GalleryNode.new(data, self)
        when 'image'
            return ImageNode.new(data, self)
        when 'interactive'
            return InteractiveNode.new(data, self)
        when 'reference'
            # go pull the referenced item and create appropriate node 
            # with attached image as a child
            node = create(@db.get(data['target']['id']))
            if data.has_key?('image')
                node << create(data['image'])
            end
            return node
        end
    end
end
